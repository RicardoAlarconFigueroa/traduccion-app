package com.nuuptech.traducir.app.backend.apirest.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.amazonaws.services.comprehend.AmazonComprehend;
import com.amazonaws.services.comprehend.model.DetectDominantLanguageRequest;
import com.amazonaws.services.comprehend.model.DetectDominantLanguageResult;
import com.amazonaws.services.translate.AmazonTranslate;
import com.amazonaws.services.translate.model.TranslateTextRequest;
import com.amazonaws.services.translate.model.TranslateTextResult;
import com.nuuptech.traducir.app.backend.apirest.models.entity.Traduccion;

public interface ITraduccionService {
	
	public AmazonTranslate clienteTranslate();
	
	public AmazonComprehend clienteComprehend();
	
	public TranslateTextRequest datosTraduccion(Traduccion traduccion);
	
	public DetectDominantLanguageRequest idiomaDominante (String texto);
	
	public TranslateTextResult traduccion(AmazonTranslate translate,TranslateTextRequest traduccion);
	
	public DetectDominantLanguageResult idiomaResul (AmazonComprehend comprehend, DetectDominantLanguageRequest request);
	
	public List<Traduccion> findAll();
	
	public Page<Traduccion> findAll(Pageable pageable);
	
	public Traduccion findById(Long id);
	
	public Traduccion save(Traduccion traduccion);
	
	public void delete(Long id);
}
