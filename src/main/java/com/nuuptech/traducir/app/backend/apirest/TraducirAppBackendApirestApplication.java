package com.nuuptech.traducir.app.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TraducirAppBackendApirestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TraducirAppBackendApirestApplication.class, args);
	}

}
