package com.nuuptech.traducir.app.backend.apirest.models.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.nuuptech.traducir.app.backend.apirest.models.entity.Usuario;

public interface IUsuarioDao extends JpaRepository<Usuario, Long>{

	public List<Usuario> findAllByOrderByIdAsc();
	
	public Page<Usuario> findAllByOrderByIdAsc(Pageable pageable);
}
