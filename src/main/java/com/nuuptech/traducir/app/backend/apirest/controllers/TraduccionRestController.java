package com.nuuptech.traducir.app.backend.apirest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.translate.AmazonTranslate;
import com.amazonaws.services.translate.model.TranslateTextRequest;
import com.amazonaws.services.translate.model.TranslateTextResult;
import com.nuuptech.traducir.app.backend.apirest.models.entity.Traduccion;
import com.nuuptech.traducir.app.backend.apirest.models.services.TraduccionServiceImpl;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class TraduccionRestController {

	@Autowired
	private TraduccionServiceImpl traduccionService;

	@GetMapping("/traducciones")
	public List<Traduccion> findAll() {
		return traduccionService.findAll();
	}
	
	@GetMapping("/traducciones/page/{page}")
	public Page<Traduccion> findAll(@PathVariable Integer page){
		return traduccionService.findAll(PageRequest.of(page, 5));
	}

	@GetMapping("/traducciones/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {

		Traduccion traduccion = null;
		Map<String, Object> response = new HashMap<>();

		try {
			traduccion = traduccionService.findById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (traduccion == null) {
			response.put("mensaje", "El traducion con ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Traduccion>(traduccion, HttpStatus.OK);
	}

	@PostMapping("/traducciones")
	public ResponseEntity<?> create(@Valid @RequestBody Traduccion traduccion, BindingResult result) {

		AmazonTranslate clienteTranslate = null;
		Traduccion traduccionNueva = null;
		TranslateTextRequest request = null;
		TranslateTextResult resultTranslate = null;
		
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			List<String> errors = new ArrayList<>();

			for (FieldError err : result.getFieldErrors()) {
				errors.add("El campo '" + err.getField() + "' " + err.getDefaultMessage());
			}
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {
			clienteTranslate = traduccionService.clienteTranslate();
			request = traduccionService.datosTraduccion(traduccion);
			resultTranslate = traduccionService.traduccion(clienteTranslate, request);
			
			traduccion.setTextoSalida(resultTranslate.getTranslatedText());
			
			traduccionNueva = traduccionService.save(traduccion);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje",
				"La traduccion  con ID: ".concat(traduccion.getId().toString().concat(" ha sido creado con exito!")));
		response.put("traduccion", traduccionNueva);
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	@PutMapping("/traducciones/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Traduccion traduccion, BindingResult result,
			@PathVariable Long id) {

		AmazonTranslate clienteUpdated = null;

		TranslateTextRequest requestUpdated = null;

		TranslateTextResult resultUpdate = null;

		Traduccion traduccionActual = traduccionService.findById(id);

		Traduccion traduccionUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {

			List<String> errors = new ArrayList<>();

			for (FieldError err : result.getFieldErrors()) {
				errors.add("El campo '" + err.getField() + "' " + err.getDefaultMessage());
			}

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (traduccionActual == null) {
			response.put("mensaje", "Error: no se puede editar la traduccion ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		try {
			traduccionActual.setOrigen(traduccion.getOrigen());
			traduccionActual.setDestino(traduccion.getDestino());
			traduccionActual.setTexto(traduccion.getTexto());

			clienteUpdated = traduccionService.clienteTranslate();
			requestUpdated = traduccionService.datosTraduccion(traduccionActual);
			resultUpdate = traduccionService.traduccion(clienteUpdated, requestUpdated);

			traduccionActual.setTextoSalida(resultUpdate.getTranslatedText());
			traduccionUpdated = traduccionService.save(traduccionActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje",
				"La traduccion con ID: ".concat(id.toString().concat(" ha sido actualizado con exito!")));
		response.put("traduccion", traduccionUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/traducciones/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id){
		
		Map<String, Object> response = new HashMap<>();
		
		try {
			traduccionService.delete(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar la traduccion de la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La traduccion ha sido eliminada con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}