package com.nuuptech.traducir.app.backend.apirest.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.comprehend.AmazonComprehend;
import com.amazonaws.services.comprehend.AmazonComprehendClientBuilder;
import com.amazonaws.services.comprehend.model.DetectDominantLanguageRequest;
import com.amazonaws.services.comprehend.model.DetectDominantLanguageResult;
import com.amazonaws.services.translate.AmazonTranslate;
import com.amazonaws.services.translate.AmazonTranslateClient;
import com.amazonaws.services.translate.model.TranslateTextRequest;
import com.amazonaws.services.translate.model.TranslateTextResult;
import com.nuuptech.traducir.app.backend.apirest.models.dao.ITraduccionDao;
import com.nuuptech.traducir.app.backend.apirest.models.entity.Traduccion;

@Service
public class TraduccionServiceImpl implements ITraduccionService {

	@Autowired
	private ITraduccionDao traduccionDao;

	@Override
	@Transactional
	public AmazonTranslate clienteTranslate() {

		AWSCredentialsProvider awsCreds = DefaultAWSCredentialsProviderChain.getInstance();

		AmazonTranslate translate = AmazonTranslateClient.builder()
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds.getCredentials()))
				.withRegion(Regions.US_EAST_1).build();

		return translate;
	}

	@Override
	@Transactional
	public AmazonComprehend clienteComprehend() {

		AWSCredentialsProvider awsCreds = DefaultAWSCredentialsProviderChain.getInstance();

		AmazonComprehend comprehend = AmazonComprehendClientBuilder.standard()
				.withCredentials(awsCreds)
				.withRegion(Regions.US_EAST_1).build();

		return comprehend;
	}

	@Override
	@Transactional
	public TranslateTextRequest datosTraduccion(Traduccion traduccion) {

		TranslateTextRequest request = new TranslateTextRequest()
				.withText(traduccion.getTexto())
				.withSourceLanguageCode(traduccion.getOrigen())
				.withTargetLanguageCode(traduccion.getDestino());

		return request;
	}

	@Override
	@Transactional
	public DetectDominantLanguageRequest idiomaDominante(String texto) {

		DetectDominantLanguageRequest request = new DetectDominantLanguageRequest().withText(texto);

		return request;
	}

	@Override
	@Transactional
	public TranslateTextResult traduccion(AmazonTranslate translate, TranslateTextRequest traduccion) {
		return translate.translateText(traduccion);
	}

	@Override
	@Transactional
	public DetectDominantLanguageResult idiomaResul(AmazonComprehend comprehend,
			DetectDominantLanguageRequest request) {
		return comprehend.detectDominantLanguage(request);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Traduccion> findAll() {
		return (List<Traduccion>) traduccionDao.findAllByOrderByIdAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Traduccion> findAll(Pageable pageable) {
		return traduccionDao.findAllByOrderByIdAsc(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Traduccion findById(Long id) {
		return traduccionDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Traduccion save(Traduccion traduccion) {
		return traduccionDao.save(traduccion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		traduccionDao.deleteById(id);
	}

}
