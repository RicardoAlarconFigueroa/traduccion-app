package com.nuuptech.traducir.app.backend.apirest.models.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.nuuptech.traducir.app.backend.apirest.models.entity.Traduccion;

public interface ITraduccionDao extends JpaRepository<Traduccion, Long>{

	public List<Traduccion> findAllByOrderByIdAsc();
	
	public Page<Traduccion> findAllByOrderByIdAsc(Pageable pageable);
}
