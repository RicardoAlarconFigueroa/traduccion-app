package com.nuuptech.traducir.app.backend.apirest.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "traducciones")
public class Traduccion implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "no puede estar vacio !")
	@Size(min = 2, max = 2, message = "el tamaño solo debe de ser de 2 caracteres !")
	@Column(nullable = false)
	private String origen;

	@NotEmpty(message = "no puede estar vacio !")
	@Size(min = 2, max = 2, message = "el tamaño solo debe de ser de 2 caracteres !")
	@Column(nullable = false)
	private String destino;

	@NotEmpty(message = "no puede estar vacio !")
	@Column(nullable = false, length=2500)
	private String texto;
	
	@Column(length=2500)
	private String textoSalida;

	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;

	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public String getTextoSalida() {
		return textoSalida;
	}

	public void setTextoSalida(String textoSalida) {
		this.textoSalida = textoSalida;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
