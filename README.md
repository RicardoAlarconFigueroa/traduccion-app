# traduccion-app

Back para Traducir texto por medio de Aws Translate

### Building

    mvn clean package

### Running Locally

    mvn spring-boot:run

## Running on OpenShift

    mvn fabric8:deploy
